#!/bin/bash

#=======================================================================================
#                Accenture do Brasil Ltda.
# Autor - Eliel dos teclados
# Data - 24/08/2022
# Resumo - O script consiste em realizar o deploy de uma classe com campos customizados
# atrav�s do arquivo podl e executar 4 arquivos NAP utilizando os campos adicionados
#=======================================================================================

PROCESSNAME=04_Faturar_Impostacao_US3_1
VERSION="0.1"
SHELLDIR="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

source /home/pin/brm/BRM/bin/util/functions.inc.sh



### Realizar o parse dos custom fields
cd $PIN_HOME/include
parse_custom_ops_fields.pl -L pcmc -I $PIN_HOME/include/c_flds.h -O $PIN_HOME/lib/c_flds.dat


### Configura os pin.conf e reinicia o CM e o DM se estiver em ambiente de dev
isVM
if [ $? -eq 1 ]; then
	cat $PIN_HOME/sys/test/pin.conf | grep c_flds.dat > /dev/null
	
	if [ $? -eq 1 ]; then
	echo - - ops_fields_extension_file ${PIN_HOME}/lib/c_flds.dat>> $PIN_HOME/sys/test/pin.conf
	echo "pin conf do test alterado"
	fi

	cat $PIN_HOME/sys/cm/pin.conf | grep c_flds.dat > /dev/null

	if [ $? -eq 1 ]; then
	echo - - ops_fields_extension_file ${PIN_HOME}/lib/c_flds.dat>> $PIN_HOME/sys/cm/pin.conf
	echo "pin conf do cm alterado"
	fi
	
	stopDMCM
	startDMCM
fi


### Realiza o pin_deploy da classe
cd $PIN_HOME/sys/test/
podlApply config_c_dados_filiais.podl

### Executa os NAPs
testnap $CONFDIR/create_config_c_dados_filiais.nap > dados_filiais.log 2>erros_dados_filiais.log
testnap $CONFDIR/create_config_c_dados_cobilling.nap > dados_cobilling.log 2>erros_dados_cobilling.log
testnap $CONFDIR/create_billing_sequence_nf_no.nap > sequencia_filiais.log 2>erros_sequencia_filiais.log
testnap $CONFDIR/create_cobilling_sequence_nf_no.nap > sequencia_cobilling.log 2>erros_sequencia_cobilling.log

