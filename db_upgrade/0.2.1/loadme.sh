# Copyright (c) 2020 Oracle Corporation and/or its affiliates. All rights reserved.
#!/bin/sh
echo -e "\e[1;32m Executando loadme.sh"
echo "event_map teste"
pwd
echo "cd /oms/sys/data/config; load_event_map -dv /oms/load/event_map"

echo -e "\e[0m FIM"
exit 0