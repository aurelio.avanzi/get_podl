#!/bin/bash

# nome e versão do script - obrigatório
PROCESSNAME=02_Catalogo
VERSION="0.1"
SHELLDIR="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd | sed -e 's/\/\//\//g')"

#funcões genéricas e carga de variaveis default
if [  ! -f "${PIN_HOME}/bin/util/functions.inc.sh" ]
then
	echo "ERROR: functions.inc.sh not found";
	exit -1;
fi
. ${PIN_HOME}/bin/util/functions.inc.sh

linfo "Starting: $PROCESSNAME $VERSION"

# prereq
checkEnv

stopDMCM
startDMCM

# prereq - DM e CM precisam estar rodando
checkDMCMRunning

linfo "Criar configuração dos Serviços VIVO do BRM"

cd $PIN_HOME/sys/data/config

linfo "Criar currency (pin_beid)"

fn_load_pin_beid -v ${CONFDIR}/ac_pin_beid

retr=$?
if [ $retr -ne 0 ]; then
     linfo "ERROR: load_pin_beid return error $retr"
     exit -1
else
      linfo "SUCESS: load_pin_beid"
fi

linfo "Carga catalogo BRM"

pwd

fn_loadpricelist -v -d -f -c ${CONFDIR}/ac_pricelist.products.xml 

retr=$?
if [ $retr -ne 0 ]; then
     linfo "ERROR: loadpricelist return error $retr"
     exit -1
else
      linfo "SUCESS: loadpricelist"
fi

fn_loadpricelist -v -d -f -c ${CONFDIR}/ac_pricelist.deals.xml 

retr=$?
if [ $retr -ne 0 ]; then
     linfo "ERROR: loadpricelist return error $retr"
     exit -1
else
      linfo "SUCESS: loadpricelist"
fi

fn_loadpricelist -v -d -f -c ${CONFDIR}/ac_pricelist.plans.xml 

retr=$?
if [ $retr -ne 0 ]; then
     linfo "ERROR: loadpricelist return error $retr"
     exit -1
else
      linfo "SUCESS: loadpricelist"
fi

fn_loadpricelist -v -d -f -c ${CONFDIR}/ac_pricelist.plan_lists.xml 

retr=$?
if [ $retr -ne 0 ]; then
     linfo "ERROR: loadpricelist return error $retr"
     exit -1
else
      linfo "SUCESS: loadpricelist"
fi

stopDMCM
startDMCM

# prereq - DM e CM precisam estar rodando
checkDMCMRunning
